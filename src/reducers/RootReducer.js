import { combineReducers } from "redux";
import { interpolation } from "./InterpolationReducer";
import { dates } from "./DateReducer";

export const rootReducer = combineReducers({ interpolation, dates });
