import { initialState, seriesConstants } from "../constants";
import { calculateSerie } from "../helpers/interpolation";

export function interpolation(state = initialState.interpolation, action) {
  switch (action.type) {
    case seriesConstants.GET_ALL_REQUEST:
      return { ...state, requesting: true };
    case seriesConstants.GET_ALL_SUCCESS:
      return { ...state, requesting: false, serie: action.series.M };
    case seriesConstants.GET_ALL_FAILURE:
      return { ...state, requesting: false };
    case seriesConstants.CALCULATE_SERIE:
      return {
        ...state,
        serie: calculateSerie(
          action.intervals,
          action.start,
          action.end,
          action.step
        )
      };
    case seriesConstants.SET_DATE_VALUE:
      let newSerie = JSON.parse(JSON.stringify(state.serie));
      newSerie[action.index][1] = action.value;
      return {
        ...state,
        serie: newSerie
      };
    default:
      return state;
  }
}
