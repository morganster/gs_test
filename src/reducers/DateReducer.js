import { initialState, dateConstants } from "../constants";

export function dates(state = initialState.dates, action) {
  switch (action.type) {
    case dateConstants.SET_START_DATE:
      return { ...state, startDate: [action.date, state.startDate[1]] };
    case dateConstants.SET_START_DATE_VALUE:
      return { ...state, startDate: [state.startDate[0], action.value] };
    case dateConstants.SET_END_DATE:
      return { ...state, endDate: [action.date, state.endDate[1]] };
    case dateConstants.SET_END_DATE_VALUE:
      return { ...state, endDate: [state.endDate[0], action.value] };
    case dateConstants.SET_DATE_VALUE:
      return {
        ...state,
        subIntervals: addSubinterval(
          state.subIntervals,
          action.date,
          action.value,
          action.index
        )
      };
    case dateConstants.SET_RESOLUTION:
      return { ...state, resolution: action.value, subIntervals: [] };
    default:
      return state;
  }
}

export const addSubinterval = (subintervals, date, value, index) => {
  // for now just add the array to the sub interval
  const subIndex = subintervals.findIndex(
    subinterval => subinterval.index === index
  );

  if (subIndex > -1) {
    subintervals[subIndex] = { date, value, index };
  } else {
    subintervals.push({ date, value, index });
  }
  return subintervals.sort((a, b) =>
    a.index > b.index ? 1 : b.index > a.index ? -1 : 0
  );
};
