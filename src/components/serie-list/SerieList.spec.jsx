import React from "react";
import { SerieList } from "./SerieList";
import { mount } from "enzyme";

describe("SerieList", () => {
  it("should render", () => {
    const component = mount(<SerieList dateArr={[]}></SerieList>);
    expect(component).not.toBeUndefined();
  });
});
