import React from "react";
import { DateInput } from "../date-input/DateInput";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { seriesActions } from "../../actions/DateActions";
import { Row, Col } from "reactstrap";
import { interpolationActions } from "../../actions/InterpolationAction";

export const SerieList = ({ dateArr }) => {
  const dispatch = useDispatch();
  const setDateValue = (ev, date, index) => {
    dispatch(seriesActions.setDateValue(date, ev.currentTarget.value, index));
    setTimeout(() => {
      dispatch(interpolationActions.calculateSerie());
    }, 200);
  };

  return (
    <Row>
      {dateArr.length &&
        dateArr.map((datePair, index) => (
          <Col key={`serie-${datePair[0]}`}>
            <DateInput
              date={datePair[0]}
              value={datePair[1]}
              onValueChange={ev => setDateValue(ev, datePair[0], index)}
            ></DateInput>
          </Col>
        ))}
    </Row>
  );
};

SerieList.propTypes = {
  dateArr: PropTypes.array
};
