import React from "react";
import { DateInput } from "./DateInput";
import { mount } from "enzyme";

describe("DateInput", () => {
  it("should render", () => {
    const component = mount(<DateInput></DateInput>);
    expect(component).not.toBeUndefined();
  });
});
