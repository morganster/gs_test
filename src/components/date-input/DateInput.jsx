import React from "react";
import PropTypes from "prop-types";
import { Input } from "reactstrap";

export const DateInput = ({ date, value, onDateChange, onValueChange }) => {
  const dateprops = !onDateChange
    ? { readOnly: true }
    : { onChange: onDateChange };
  return (
    <>
      <Input type="date" value={date} {...dateprops} />
      <Input type="number" value={value} onChange={onValueChange} />
    </>
  );
};

DateInput.propTypes = {
  date: PropTypes.string,
  value: PropTypes.number,
  onDateChange: PropTypes.func,
  onValueChange: PropTypes.func
};
