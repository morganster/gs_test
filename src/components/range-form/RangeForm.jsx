import React from "react";
import PropTypes from "prop-types";
import { DateInput } from "../date-input/DateInput";
import { Input, Col, Label, Button } from "reactstrap";
import { useDispatch, useSelector } from "react-redux";
import { seriesActions } from "../../actions/DateActions";
import { interpolationActions } from "../../actions/InterpolationAction";
import { resolutionList } from "../../constants";

export const RangeForm = () => {
  const dispatch = useDispatch();
  const startDate = useSelector(state => state.dates.startDate);
  const endDate = useSelector(state => state.dates.endDate);
  const Resolution = useSelector(state => state.dates.Resolution);

  const onStartDateChange = ev => {
    dispatch(seriesActions.setStartDate(ev.target.value));
  };
  const onEndDateChange = ev => {
    dispatch(seriesActions.setEndDate(ev.target.value));
  };
  const onStartDateValueChange = ev => {
    dispatch(seriesActions.setStartDateValue(ev.target.value));
  };
  const onEndDateValueChange = ev => {
    dispatch(seriesActions.setEndDateValue(ev.target.value));
  };

  const onSelectResolution = ev => {
    dispatch(seriesActions.setResolution(parseInt(ev.target.value)));
  };
  const handleClick = ev => {
    dispatch(interpolationActions.calculateSerie());
  };
  const handleClickSerie = () => {
    dispatch(interpolationActions.getSerie());
  };

  return (
    <>
      <Col>
        <DateInput
          type="date"
          date={startDate[0]}
          value={startDate[1]}
          onDateChange={onStartDateChange}
          onValueChange={onStartDateValueChange}
        />
      </Col>
      <Col>
        <DateInput
          type="date"
          date={endDate[0]}
          value={endDate[1]}
          onDateChange={onEndDateChange}
          onValueChange={onEndDateValueChange}
        />
      </Col>
      <Col>
        <Label>Resolution</Label>
        <Input
          type="select"
          onChange={onSelectResolution}
          selected={Resolution}
        >
          {resolutionList.map(resolution => (
            <option key={resolution.name} value={resolution.value}>
              {resolution.name}
            </option>
          ))}
        </Input>
        <Button onClick={handleClick}>Calculate</Button>
        <Button onClick={handleClickSerie}>Get Last Serie</Button>
      </Col>
    </>
  );
};

RangeForm.propTypes = {
  endDate: PropTypes.string,
  endDateVal: PropTypes.number,
  onValueChange: PropTypes.func
};
