import React from "react";
import { RangeForm } from "./RangeForm";
import { mount } from "enzyme";

describe("RangeForm", () => {
  it("should render", () => {
    const component = mount(<RangeForm></RangeForm>);
    expect(component).not.toBeUndefined();
  });
});
