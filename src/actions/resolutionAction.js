import { resolutionConstants } from "../constants";

const GetResolution = () => {
  return { type: resolutionConstants.GET_REQUEST };
};
const SetResolution = resolution => {
  return { type: resolutionConstants.SET_REQUEST, resolution };
};

export const resolutionActions = {
  GetResolution,
  SetResolution
};
