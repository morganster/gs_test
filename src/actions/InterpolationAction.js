import { apiUrl, seriesConstants } from "../constants";
import { seriesActions } from "./DateActions";
import axios from "axios";

const GetRequest = () => {
  return { type: seriesConstants.GET_ALL_REQUEST };
};
const GetSuccess = series => {
  return { type: seriesConstants.GET_ALL_SUCCESS, series };
};
const GetFailure = error => {
  return { type: seriesConstants.GET_ALL_FAILURE, error };
};
const PutRequest = () => {
  return { type: seriesConstants.UPDATE_ALL_REQUEST };
};
const PutSuccess = series => {
  return { type: seriesConstants.UPDATE_ALL_SUCCESS, series };
};
const PutFailure = error => {
  return { type: seriesConstants.UPDATE_ALL_FAILURE, error };
};
const SetDateValue = (date, value, index) => {
  return { type: seriesConstants.SET_DATE_VALUE, date, value, index };
};

const CalculateSerie = (intervals, start, end, step) => {
  return { type: seriesConstants.CALCULATE_SERIE, intervals, start, end, step };
};
const calculateSerie = () => {
  return (dispatch, getState) => {
    const dates = getState().dates;
    dispatch(
      CalculateSerie(
        dates.subIntervals,
        dates.startDate,
        dates.endDate,
        dates.resolution
      )
    );
    dispatch(setSerie());
  };
};
const getSerie = () => {
  return dispatch => {
    dispatch(GetRequest());
    axios
      .get(apiUrl)
      .then(series => {
        dispatch(GetSuccess(series.data));
        dispatch(seriesActions.setEndDate(series.data.endDate));
      })
      .catch(error => {
        dispatch(GetFailure(error));
      });
  };
};

const setSerie = () => {
  return (dispatch, getState) => {
    const serie = getState().interpolation.serie;
    dispatch(PutRequest());
    axios
      .put(apiUrl, { M: serie })
      .then(response => {
        dispatch(PutSuccess(response));
      })
      .catch(error => {
        dispatch(PutFailure(error));
      });
  };
};

const setDateValue = (date, value, index) => {
  return dispatch => {
    dispatch(SetDateValue(date, value, index));
  };
};

export const interpolationActions = {
  setSerie,
  getSerie,
  calculateSerie,
  setDateValue
};
