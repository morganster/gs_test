import { dateConstants } from "../constants";
import { interpolationActions } from "./InterpolationAction";

const SetStartDateValue = value => {
  return { type: dateConstants.SET_START_DATE_VALUE, value };
};
const SetEndDateValue = value => {
  return { type: dateConstants.SET_END_DATE_VALUE, value };
};
const SetStartDate = date => {
  return { type: dateConstants.SET_START_DATE, date };
};
const SetEndDate = date => {
  return { type: dateConstants.SET_END_DATE, date };
};
const SetDateValue = (date, value, index) => {
  return { type: dateConstants.SET_DATE_VALUE, date, value, index };
};
const SetResolution = value => {
  return { type: dateConstants.SET_RESOLUTION, value };
};

const SetDateValueFailure = () => {
  return { type: dateConstants.SET_DATE_VALUE_FAILURE };
};

const setStartDateValue = value => {
  return dispatch => {
    dispatch(SetStartDateValue(value));
  };
};
const setEndDate = date => {
  return dispatch => {
    dispatch(SetEndDate(date));
  };
};
const setStartDate = date => {
  return dispatch => {
    dispatch(SetStartDate(date));
  };
};

const setDateValue = (date, value, index) => {
  if (value && !isNaN(parseInt(value))) {
    return dispatch => {
      dispatch(SetDateValue(date, parseInt(value), index));
      dispatch(interpolationActions.setDateValue(date, parseInt(value), index));
    };
  } else {
    return dispatch => {
      dispatch(SetDateValueFailure());
    };
  }
};

const setEndDateValue = value => {
  return dispatch => {
    dispatch(SetEndDateValue(value));
  };
};

const setResolution = value => {
  return dispatch => {
    dispatch(SetResolution(value));
    dispatch(interpolationActions.calculateSerie());
  };
};

export const seriesActions = {
  setEndDate,
  setStartDate,
  setDateValue,
  setEndDateValue,
  setStartDateValue,
  setResolution
};
