import React from "react";
import App from "./App";
import { mount } from "enzyme";

describe("App", () => {
  it("should render", () => {
    const component = mount(<App></App>);
    expect(component).not.toBeUndefined();
  });
});
