import React from "react";
import { RangeForm } from "../components/range-form/RangeForm";
import { SerieList } from "../components/serie-list/SerieList";
import { Row } from "reactstrap";
import { useSelector } from "react-redux";

export const Home = () => {
  const endDate = new Date();
  const M = useSelector(state => state.interpolation.serie);
  const onValueChange = () => {};
  return (
    <>
      <h1>Choose Start, End date and the resolution</h1>
      <Row>
        <RangeForm
          endDate={endDate.toDateString()}
          endDateVal={10}
          onValueChange={onValueChange}
        ></RangeForm>
      </Row>
      <h2>Calulated Values</h2>
      <p>You can edit the value for any date.</p>
      <Row>
        <SerieList dateArr={M} />
      </Row>
    </>
  );
};
