import { calculateSteps } from "./intervalsCalculator";
export const getXValue = (t, m) => {
  const v = m.filter(p => p[1] <= t).pop();
  return v === undefined ? m[0][1] : v[1];
};

export const calculateSerie = (intervals, start, end, step) => {
  const serie = calculateSteps(new Date(start[0]), new Date(end[0]), step);
  let index = 0;
  let tmpStartDate = start;
  if (intervals.length > 0) {
    intervals.map(interval => {
      for (let i = index; i <= interval.index; i++) {
        serie[i] = [
          serie[i][0],
          getXValue(new Date(serie[i][0]), [
            new Date(tmpStartDate),
            [interval.date, interval.value]
          ])
        ];
        index = i + 1;
      }
    });
    for (let i = index; i < serie.length; i++) {
      serie[i] = [
        serie[i][0],
        getXValue(new Date(serie[i][0]), [new Date(tmpStartDate), end])
      ];
    }
  }
  return serie;
};
