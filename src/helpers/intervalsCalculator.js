import { formatDate } from "./dateFormater";
import { DayMs } from "../constants/date";

export const calculateSteps = (start, end, step) => {
  let result = [];
  const days = (end.getTime() - start.getTime()) / (1000 * 3600 * 24);
  let i = 1;
  while (i < days) {
    result.push([formatDate(new Date(start.getTime() + DayMs * i)), 0]);
    i = i + step;
  }
  return result;
};
