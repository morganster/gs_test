export const resolutionConstants = {
  GET_REQUEST: "RESOLUTION_GET_REQUEST",
  SET_REQUEST: "RESOLUTION_SET_REQUEST"
};

export const resolutionList = [
  { name: "Days", value: 1 },
  { name: "Week", value: 7 }
];
