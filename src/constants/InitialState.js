import { formatDate } from "../helpers/dateFormater";
import { calculateSteps } from "../helpers/intervalsCalculator";
import { DayMs } from "./date";

const formattedDate = formatDate(new Date());
const formattedDateEnd = formatDate(new Date(new Date().getTime() + 2 * DayMs));
export const initialState = {
  dates: {
    resolution: 1,
    startDate: [formattedDate, 0],
    endDate: [formattedDateEnd, 1],
    subIntervals: []
  },
  interpolation: {
    serie: calculateSteps(
      new Date(),
      new Date(new Date().getTime() + 2 * DayMs),
      1
    )
  }
};
