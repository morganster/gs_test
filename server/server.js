const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
const port = 3030;

// Where we will keep books
let M = [];
let startDate = new Date();
let endDate = new Date();

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/series", (req, res) => {
  res.send({
    M,
    startDate,
    endDate
  });
});

app.put("/series", (req, res) => {
  M = req.body.M;
  startDate = M[0][0];
  endDate = M[M.length - 1][0];

  res.send(true);
});

app.listen(port, () => console.log(`Server listening on port ${port}!`));
